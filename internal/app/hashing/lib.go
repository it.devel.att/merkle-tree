package hashing

import (
	"crypto/sha256"
	"fmt"
)

func ToHash(v string) string {
	return hashString(
		sha256.Sum256([]byte(hashString(sha256.Sum256([]byte(v))))),
	)
}

func hashString(byteHash [32]byte) string {
	return fmt.Sprintf("%x", byteHash)
}
