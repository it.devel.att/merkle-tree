package merkle_tree

import (
	"merkle-tree/internal/app/hashing"
)

type Leaf [2]string

// TODO Refactor!

// GenerateRootHash return Merkle root and Merkle tree (without root) for given slice of strings
func GenerateRootHash(hashes []string) (string, [][]string) {
	hashLeafs := make([]string, len(hashes))
	copy(hashLeafs, hashes)

	var pathToMerkleRoot [][]string
	pathToMerkleRoot = append(pathToMerkleRoot, hashes)
	for len(hashLeafs) != 1 {
		hashLeafs = generateHashLeafs(hashLeafs)

		if len(hashLeafs) != 1 {
			pathToMerkleRoot = append(pathToMerkleRoot, hashLeafs)
		}
	}
	return hashLeafs[0], pathToMerkleRoot
}

type AuthLeaf struct {
	ID   int
	Hash string
}

// GenerateMerklePath return auth path for given hash in Merkle tree
func GenerateMerklePath(hashForFind string, merkleTree [][]string) []AuthLeaf {
	idxOfTransaction, found := 0, false
	authPath := make([]AuthLeaf, 0)
	for i, txOfTransactions := range merkleTree[0] {
		if txOfTransactions == hashForFind {
			idxOfTransaction, found = i, true
		}
	}
	if !found {
		return authPath
	}
	levelIDx := make([]int, len(merkleTree))
	for level, _ := range merkleTree {
		if idxOfTransaction%2 == 1 {
			levelIDx[level] = idxOfTransaction - 1
		} else {
			levelIDx[level] = idxOfTransaction + 1
		}
		idxOfTransaction = idxOfTransaction / 2
	}
	for level, id := range levelIDx {
		if len(merkleTree[level]) == id {
			authPath = append(authPath, AuthLeaf{id, merkleTree[level][id-1]})
			continue
		}
		authPath = append(authPath, AuthLeaf{id, merkleTree[level][id]})
	}
	return authPath
}

// GenerateFromMerklePath return Merkle root by given hash and auth path in Merkle tree
func GenerateFromMerklePath(hashToFind string, authPath []AuthLeaf) string {
	result := hashToFind
	for _, authLeaf := range authPath {
		if authLeaf.ID%2 == 1 {
			result = leafToHash(Leaf{result, authLeaf.Hash})
			continue
		}
		result = leafToHash(Leaf{authLeaf.Hash, result})
	}
	return result
}

func generateHashLeafs(hashes []string) []string {
	if len(hashes) < 2 {
		return hashes
	}
	if len(hashes)%2 == 1 {
		hashes = append(hashes, hashes[len(hashes)-1])
	}
	var leafHashes []string
	for i, _ := range hashes {
		if i%2 == 1 || i == len(hashes) {
			continue
		}
		leafHashes = append(leafHashes, leafToHash(Leaf{hashes[i], hashes[i+1]}))
	}
	return leafHashes
}

func leafToHash(leaf Leaf) string {
	return hashing.ToHash(leaf[0] + leaf[1])
}
